package com.example.coronatracker.ui.allCases

import android.content.Context
import android.preference.PreferenceManager

object SharedPrefs {

    fun setMockStatus(key: String, value: Boolean, context: Context) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = preferences.edit()
        editor.putBoolean(key, value)
        editor.apply()
    }

    fun getMockStatus(key: String, context: Context): Boolean {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        return preferences.getBoolean(key, false)
    }
}