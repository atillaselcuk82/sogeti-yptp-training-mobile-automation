package com.example.coronatracker.ui.filter

data class CoronaPerCountry(
    val active: Int,
    val cases: Int,
    val casesPerOneMillion: Int,
    val country: String,
    val critical: Int,
    val deaths: Int,
    val deathsPerOneMillion: Int,
    val recovered: Int,
    val todayCases: Int,
    val todayDeaths: Int
)