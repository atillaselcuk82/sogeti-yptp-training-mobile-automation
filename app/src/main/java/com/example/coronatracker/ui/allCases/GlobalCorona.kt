package com.example.coronatracker.ui.allCases

class GlobalCorona(
    var cases: Int,
    var deaths: Int,
    var recovered: Int
)