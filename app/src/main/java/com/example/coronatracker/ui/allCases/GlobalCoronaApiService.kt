package com.example.coronatracker.ui.allCases

import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface GlobalCoronaApiService {
    @GET("all")
    fun getAllCoronaCases(): Observable<GlobalCorona>
    companion object {
        fun create(): GlobalCoronaApiService {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://coronavirus-19-api.herokuapp.com/")
                .build()
            return retrofit.create(GlobalCoronaApiService::class.java)
        }
    }
}