package com.example.coronatracker.ui.filter

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.SeekBar
import android.widget.Spinner
import android.widget.TextView
import androidx.appcompat.widget.SwitchCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.coronatracker.MainActivity
import com.example.coronatracker.R
import com.google.android.material.floatingactionbutton.FloatingActionButton

class FilterFragment : Fragment() {

    var seekBarValue = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_filter, container, false)

        val sortBySpinner: Spinner = root.findViewById(R.id.sort_by_spinner)
        val showCountriesSpinner: Spinner = root.findViewById(R.id.show_countries_spinner)
        val displayDetailsSwitch: SwitchCompat = root.findViewById(R.id.display_details_switch)
        val seekBarTextView: TextView = root.findViewById(R.id.seek_bar_value_tv)
        val deathRatioSeekBar: SeekBar = root.findViewById(R.id.death_ratio_seek_bar)
        val searchFab: FloatingActionButton = root.findViewById(R.id.fab_search)

        setSpinner(root.context, sortBySpinner, R.array.sort_by)
        setSpinner(root.context, showCountriesSpinner, R.array.show_countries)
        initializeWidgets(seekBarTextView, displayDetailsSwitch)
        setSeekbar(deathRatioSeekBar, seekBarTextView)

        searchFab.setOnClickListener {
           goToCoronaByCountryListFragment(sortBySpinner, showCountriesSpinner, displayDetailsSwitch)
        }

        return root
    }

    private fun initializeWidgets(seekBarTextView: TextView, switch: SwitchCompat) {
        seekBarTextView.text = 0.toString()
        switch.isChecked = false
    }

    private fun setSeekbar(deathRatioSeekBar: SeekBar, seekBarTextView: TextView) {
        deathRatioSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {

            override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
                seekBarValue = i
                seekBarTextView.text = i.toString()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })
    }

    private fun setSpinner(context: Context, spinner: Spinner, arrayId: Int) {
        val list = resources.getStringArray(arrayId)
        val layout = R.layout.spinner_item
        val adapter = ArrayAdapter(context, layout, list)
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        spinner.adapter = adapter
    }

    private fun goToCoronaByCountryListFragment(sortBy: Spinner, showCountries: Spinner, displayDetails: SwitchCompat) {
        val intent = Intent(activity,CoronaByCountryListActivity::class.java)
        intent.putExtra("sortBy",sortBy.selectedItem.toString())
        intent.putExtra("showCountries", showCountries.selectedItem.toString())
        intent.putExtra("isChecked", displayDetails.isChecked)
        intent.putExtra("highlightFrom", seekBarValue)
        startActivity(intent)

//        val bundle = Bundle()
//        bundle.putString("sortBy", sortBy.selectedItem.toString())
//        bundle.putString("showCountries", showCountries.selectedItem.toString())
//        bundle.putBoolean("displayDetails", displayDetails.isChecked)
//        bundle.putInt("highlightFrom", seekBarValue)
//
//        val fragment = CoronaByCountryListFragment()
//        fragment.arguments = bundle
//
//        val fragmentManager = activity?.supportFragmentManager
//        val fragmentTransaction = fragmentManager?.beginTransaction()
//        fragmentTransaction?.replace(R.id.nav_host_fragment, fragment)
//        fragmentTransaction?.addToBackStack(null)
//        fragmentTransaction?.commit()
    }

}