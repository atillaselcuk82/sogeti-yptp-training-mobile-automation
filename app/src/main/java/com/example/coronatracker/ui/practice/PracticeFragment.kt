package com.example.coronatracker.ui.practice

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import com.example.coronatracker.R
import java.util.*
import java.util.regex.Pattern

class PracticeFragment : Fragment() {

    private var isMissing: Boolean = false
    private var isIncorrect: Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_practice, container, false)
        val name: EditText = root.findViewById(R.id.editTextName)
        val birthday: EditText = root.findViewById(R.id.editTextDate)
        val gender: RadioGroup = root.findViewById(R.id.genderRadioGroup)
        val swimming: CheckBox = root.findViewById(R.id.checkBoxSwimming)
        val football: CheckBox = root.findViewById(R.id.checkBoxFootball)
        val dancing: CheckBox = root.findViewById(R.id.checkBoxDancing)
        val fishing: CheckBox = root.findViewById(R.id.checkBoxFishing)
        val buttonCalculate: Button = root.findViewById(R.id.buttonSummary)
        val summaryText: TextView = root.findViewById(R.id.textViewSummary)
        val summaryHobbiesText: TextView = root.findViewById(R.id.textViewSummaryHobbies)
        val swimmingImage: ImageView = root.findViewById(R.id.imageViewSwimming)
        val footballImage: ImageView = root.findViewById(R.id.imageViewFootball)
        val dancingImage: ImageView = root.findViewById(R.id.imageViewDancing)
        val fishingImage: ImageView = root.findViewById(R.id.imageViewFishing)

        summaryText.text = ""
        summaryHobbiesText.text = ""
        makeHobbiesInvisible(swimmingImage, footballImage, dancingImage, fishingImage)

        buttonCalculate.setOnClickListener{
            makeHobbiesInvisible(swimmingImage, footballImage, dancingImage, fishingImage)
            isMissing = missingEntries(name, birthday, gender)
            isIncorrect = incorrectEntry(birthday)
            if (!isMissing && !isIncorrect) {
                displaySummaryText(summaryText, summaryHobbiesText, name, birthday, gender, swimming, football, dancing, fishing, root)
                makeHobbiesVisible(swimmingImage, footballImage, dancingImage, fishingImage, swimming, football, dancing, fishing)
            } else if (isMissing) {
                missingText(summaryText)
            } else {
                incorrectBirthday(summaryText)
            }
        }
        return root
    }

    fun makeHobbiesInvisible(swimmingImage: ImageView, footballImage: ImageView, dancingImage: ImageView, fishingImage: ImageView) {
        swimmingImage.visibility = View.INVISIBLE
        footballImage.visibility = View.INVISIBLE
        dancingImage.visibility = View.INVISIBLE
        fishingImage.visibility = View.INVISIBLE
    }

    fun displaySummaryText(summaryText: TextView,
                           summaryHobbiesText: TextView,
                           name: EditText,
                           birthday: EditText,
                           gender: RadioGroup,
                           swimming: CheckBox,
                           football: CheckBox,
                           dancing: CheckBox,
                           fishing: CheckBox,
                           root: View) {
        "Hi, ${getTitle(gender, root)} ${name.text}. You are ${getAge(birthday)} year(s) old!".also { summaryText.text = it }
        summaryHobbiesText.text = getString(R.string.hobbies)
    }

    private fun getTitle(gender: RadioGroup, root: View): String {
        val selectedId = gender.checkedRadioButtonId
        val radioButton = root.findViewById<RadioButton>(selectedId)
        return if (radioButton.text.toString() == "Male") "Mr." else "Ms."
    }

    fun getAge(birthday: EditText): Int {
        val bDay = birthday.text.substring(8).toInt()
        val bMonth = birthday.text.substring(5, 7).toInt()
        val bYear = birthday.text.substring(0, 4).toInt()

        val dob = Calendar.getInstance()
        val today =  Calendar.getInstance()

        dob.set(bYear, bMonth-1, bDay)

        var age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR)
        val currentDay = today.get(Calendar.DAY_OF_MONTH)
        val currentMonth = today.get(Calendar.MONTH) + 1

        if (currentMonth < bMonth || (currentMonth == bMonth && currentDay < bDay)) {
            age--
        }
        return age
    }

    fun makeHobbiesVisible(swimmingImage: ImageView,
                           footballImage: ImageView,
                           dancingImage: ImageView,
                           fishingImage: ImageView,
                           swimming: CheckBox,
                           football: CheckBox,
                           dancing: CheckBox,
                           fishing: CheckBox) {
        if (swimming.isChecked) swimmingImage.visibility = View.VISIBLE
        if (football.isChecked) footballImage.visibility = View.VISIBLE
        if (dancing.isChecked) dancingImage.visibility = View.VISIBLE
        if (fishing.isChecked) fishingImage.visibility = View.VISIBLE
    }

    fun missingEntries(name: EditText, birthday: EditText, gender: RadioGroup): Boolean {
       return (name.text.isEmpty() || birthday.text.isEmpty() || gender.checkedRadioButtonId == -1)
    }

    fun incorrectEntry(birthday: EditText): Boolean {
        var incorrect: Boolean
        val datePattern: Pattern = Pattern.compile(
            "^((2000|2400|2800|(19|2[0-9](0[48]|[2468][048]|[13579][26])))-02-29)$"
                    + "|^(((19|2[0-9])[0-9]{2})-02-(0[1-9]|1[0-9]|2[0-8]))$"
                    + "|^(((19|2[0-9])[0-9]{2})-(0[13578]|10|12)-(0[1-9]|[12][0-9]|3[01]))$"
                    + "|^(((19|2[0-9])[0-9]{2})-(0[469]|11)-(0[1-9]|[12][0-9]|30))$"
        )
        val dob = Calendar.getInstance()
        val today =  Calendar.getInstance()

        if (!datePattern.matcher(birthday.text).matches()) {
            incorrect = true
        } else {
            incorrect = false

            val bDay = birthday.text.substring(8).toInt()
            val bMonth = birthday.text.substring(5, 7).toInt()
            val bYear = birthday.text.substring(0, 4).toInt()

            dob.set(bYear, bMonth-1, bDay)
            if (dob.time.after(today.time)) {
                incorrect = true
            }
        }
        return incorrect
    }

    fun missingText(summaryText: TextView) {
        summaryText.text = getString(R.string.missing_text)
    }

    fun incorrectBirthday(summaryText: TextView) {
        summaryText.text = getString(R.string.incorrect_birthday)
    }
}


