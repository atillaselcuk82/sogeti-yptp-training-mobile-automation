package com.example.coronatracker.ui.filter

import android.graphics.Color
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.coronatracker.R

class CountryListAdapter(private val items: ArrayList<CoronaPerCountry>,
                         private val displayDetailsIsChecked: Boolean,
                         val highlightFrom: Int) :
    RecyclerView.Adapter<CountryListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v: View = if (displayDetailsIsChecked) {
            LayoutInflater.from(parent.context).inflate(R.layout.country_item_detailed, parent, false)
        } else {
            LayoutInflater.from(parent.context).inflate(R.layout.country_item, parent, false)
        }
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(country: CoronaPerCountry) {
            val countryName = itemView.findViewById(R.id.county_name_tv) as TextView
            val totalCases = itemView.findViewById(R.id.cases_tv) as TextView
            val casesToday = itemView.findViewById(R.id.today_cases_tv) as TextView
            val activeCases = itemView.findViewById(R.id.active_cases_tv) as TextView
            val totalDeaths = itemView.findViewById(R.id.deaths_tv) as TextView
            val deathsToday = itemView.findViewById(R.id.today_deaths_tv) as TextView
            val deathRatio = itemView.findViewById(R.id.death_ratio_tv) as TextView
            val recovered = itemView.findViewById(R.id.recovered_tv) as TextView
            val critical = itemView.findViewById(R.id.critical_tv) as TextView
            val dRatio = country.deaths.toDouble() / country.recovered.toDouble()

            countryName.text = country.country
            totalCases.text = "Cases: ${country.cases}"
            casesToday.text = "Today: ${country.todayCases}"
            activeCases.text = "Active: ${country.active}"
            totalDeaths.text = "Deaths: ${country.deaths}"
            deathsToday.text = "Today: ${country.todayDeaths}"
            deathRatio.text = "Death-ratio: ${"%.2f".format(dRatio)}"
            recovered.text = "Recovered: ${country.recovered}"
            critical.text = "Critical: ${country.critical}"

            if (displayDetailsIsChecked) {
                val casesPerMillion = itemView.findViewById(R.id.cases_per_million_tv) as TextView
                val deathsPerMillion = itemView.findViewById(R.id.deaths_per_million_tv) as TextView
                casesPerMillion.text = "Cases/Million: ${country.casesPerOneMillion}"
                deathsPerMillion.text = "Deaths/Million: ${country.deathsPerOneMillion}"
            }

            if (dRatio > highlightFrom && highlightFrom != 0) {
                deathRatio.setTypeface(null, Typeface.BOLD)
                deathRatio.setTextColor(Color.parseColor("#FF0000"))
            }
        }
    }
}