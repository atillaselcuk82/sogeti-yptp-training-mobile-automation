package com.example.coronatracker.ui.filter

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.coronatracker.R
import com.example.coronatracker.ui.allCases.SharedPrefs.getMockStatus
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class CoronaByCountryListActivity : AppCompatActivity() {

    private val coronaPerCountryApiServe by lazy { CoronaPerCountryApiService.create() }
    private var disposable: Disposable? = null
    private lateinit var adapter : CountryListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_corona_by_country_list)
        val countryListRecyclerView: RecyclerView = findViewById(R.id.country_list_rec_view)
        fetchData(countryListRecyclerView)

        val swipeRefresh: SwipeRefreshLayout = findViewById(R.id.swiperefresh_country_list)
        swipeRefresh.setOnRefreshListener {
            fetchData(countryListRecyclerView)
            swipeRefresh.isRefreshing = false
        }
    }

    private fun fetchData(recView: RecyclerView) {
        if (getMockStatus("mock", this)) {
            fetchMockedData(recView)
        } else {
            disposable = coronaPerCountryApiServe.getCountryDetails()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { result -> populateRecyclerView(recView, filterAndSort(result)) },
                    { error -> Toast.makeText(this, error.message, Toast.LENGTH_SHORT).show() }
                )
        }
    }

    private fun fetchMockedData(recView: RecyclerView) {
        val json: String? = this.assets.open("countries.json").bufferedReader().use { it.readText() }
        val coronaPerCountry = object : TypeToken<List<CoronaPerCountry>>() {}.type
        val logs = Gson().fromJson<ArrayList<CoronaPerCountry>>(json, coronaPerCountry)
        populateRecyclerView(recView, (filterAndSort(logs)))
    }


    private fun filterAndSort(countryList: ArrayList<CoronaPerCountry>): ArrayList<CoronaPerCountry> {
        var list = countryList.toList()
        val sortBy=intent.getStringExtra("sortBy")
        val showCountries=intent.getStringExtra("showCountries")

        list = when(sortBy) {
            "deaths" -> list.sortedByDescending { it.deaths }
            "recovered" -> list.sortedByDescending { it.recovered }
            else -> list.sortedByDescending { it.cases }
        }

        list = when(showCountries) {
            "top 10" -> list.filterIndexed { index, _ -> (index < 10) }
            "top 3" -> list.filterIndexed { index, _ -> (index < 3) }
            else -> list.filterIndexed { index, _ -> (index >= 0) }
        }
        return ArrayList(list)
    }

    private fun populateRecyclerView(recView: RecyclerView, countryList: ArrayList<CoronaPerCountry>) {
        val showDetail = intent.getBooleanExtra("isChecked", false)
        val highLightFrom = intent.getIntExtra("highlightFrom", 0)
        recView.layoutManager = LinearLayoutManager(this)
        adapter = CountryListAdapter(countryList, showDetail, highLightFrom)
        recView.adapter = adapter
    }

    override fun onPause() {
        super.onPause()
        disposable?.dispose()
    }
}
