package com.example.coronatracker.ui.allCases

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.coronatracker.R
import com.example.coronatracker.ui.allCases.SharedPrefs.getMockStatus
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class AllCasesFragment : Fragment() {

    private val globalCoronaApiServe by lazy { GlobalCoronaApiService.create() }
    private var isMock: Boolean = false
    private var disposable: Disposable? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_all_cases, container, false)

        val allCases: TextView = root.findViewById(R.id.number_of_all_corona_cases_tv)
        val deaths: TextView = root.findViewById(R.id.number_of_deaths_tv)
        val recovered: TextView = root.findViewById(R.id.number_of_recovered_tv)

        val swipeRefresh: SwipeRefreshLayout = root.findViewById(R.id.swiperefresh)
        swipeRefresh.setOnRefreshListener {
            fetchDataFromEndpoint(allCases, deaths, recovered)
            swipeRefresh.isRefreshing = false
        }
        isMock = getMockStatus("mock", root.context)

        if (isMock) fetchMockedData(allCases, deaths, recovered)
        else fetchDataFromEndpoint(allCases, deaths, recovered)

        return root
    }

    private fun fetchDataFromEndpoint(allCases: TextView, deaths: TextView, recovered: TextView) {
        disposable = globalCoronaApiServe.getAllCoronaCases()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    allCases.text = "${result.cases}"
                    deaths.text = "${result.deaths}"
                    recovered.text = "${result.recovered}"
                },
                { error -> Toast.makeText(context, error.message, Toast.LENGTH_SHORT).show() }
            )
    }

    private fun fetchMockedData(allCases: TextView, deaths: TextView, recovered: TextView) {
        val json: String? = activity?.assets?.open("all.json")
            ?.bufferedReader().use { it?.readText() }
        val result = Gson().fromJson(json, GlobalCorona::class.java)

        allCases.text = "${result.cases}"
        deaths.text = "${result.deaths}"
        recovered.text = "${result.recovered}"
    }

    override fun onPause() {
        super.onPause()
        disposable?.dispose()
    }
}

