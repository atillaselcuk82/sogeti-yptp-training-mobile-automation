package com.example.coronatracker.ui.filter

import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface CoronaPerCountryApiService {
    @GET("countries")
    fun getCountryDetails(): Observable<ArrayList<CoronaPerCountry>>
    companion object {
        fun create(): CoronaPerCountryApiService {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://coronavirus-19-api.herokuapp.com/")
                .build()
            return retrofit.create(CoronaPerCountryApiService::class.java)
        }
    }
}

