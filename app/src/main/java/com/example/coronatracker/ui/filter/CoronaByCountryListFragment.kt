package com.example.coronatracker.ui.filter

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.coronatracker.R
import com.example.coronatracker.ui.allCases.SharedPrefs.getMockStatus
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class CoronaByCountryListFragment : Fragment() {

    private val coronaPerCountryApiServe by lazy { CoronaPerCountryApiService.create() }
    private var disposable: Disposable? = null
    private lateinit var adapter : CountryListAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_corona_by_country_list, container, false)
        val countryListRecyclerView: RecyclerView = root.findViewById(R.id.country_list_rec_view)
        val swipeRefresh: SwipeRefreshLayout = root.findViewById(R.id.swiperefresh_country_list)

        fetchData(countryListRecyclerView, root.context)
        swipeRefresh.setOnRefreshListener {
            fetchData(countryListRecyclerView, root.context)
            swipeRefresh.isRefreshing = false
        }
        return root
    }

    private fun fetchData(recView: RecyclerView, context: Context) {
        if (getMockStatus("mock", context)) {
            fetchMockedData(recView, context)
        } else {
            disposable = coronaPerCountryApiServe.getCountryDetails()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { result -> populateRecyclerView(recView, context, filterAndSort(result)) },
                    { error -> Toast.makeText(context, error.message, Toast.LENGTH_SHORT).show() }
                )
        }
    }

    private fun fetchMockedData(recView: RecyclerView, context: Context) {
        val json: String? = activity?.assets?.open("countries.json")
            ?.bufferedReader().use { it?.readText() }
        val coronaPerCountry = object : TypeToken<List<CoronaPerCountry>>() {}.type
        val logs = Gson().fromJson<ArrayList<CoronaPerCountry>>(json, coronaPerCountry)
        populateRecyclerView(recView, context, (filterAndSort(logs)))
    }


    private fun filterAndSort(countryList: ArrayList<CoronaPerCountry>): ArrayList<CoronaPerCountry> {
        var list = countryList.toList()
        val sortBy = arguments?.getString("sortBy")
        val showCountries = arguments?.getString("showCountries")

        list = when(sortBy.toString())   {
            "deaths" -> list.sortedByDescending { it.deaths }
            "recovered" -> list.sortedByDescending { it.recovered }
            else -> list.sortedByDescending { it.cases }
        }

        list = when(showCountries.toString()) {
            "top 10" -> list.filterIndexed { index, _ -> (index < 10) }
            "top 3" -> list.filterIndexed { index, _ -> (index < 3) }
            else -> list.filterIndexed { index, _ -> (index >= 0) }
        }
        return ArrayList(list)
    }

    private fun populateRecyclerView(
        recView: RecyclerView,
        context: Context,
        countryList: ArrayList<CoronaPerCountry>
    ) {
        val displayDetail = arguments?.getBoolean("displayDetails")
        val highLightFrom = arguments?.getInt("highlightFrom")
        recView.layoutManager = LinearLayoutManager(context)
        adapter = CountryListAdapter(countryList, displayDetail!!, highLightFrom!!)
        recView.adapter = adapter
    }

    override fun onPause() {
        super.onPause()
        disposable?.dispose()
    }
}